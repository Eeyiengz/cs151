import java.util.HashMap;
import java.util.Map;

/**
 * The MailSystem manages the users and their mailbox(s).
 * Allows a single user access at a time.
 * Allows sending messages to other accounts
 * Allows the current logged in user access to their received messages
 */
public class MailSystem {
    private boolean loggedIn;
    private User currentUser;
    private Mailbox userMailbox;
    private Map<String, User> users;

    public final String GREETING = "Welcome to TextChat.";
    public final String HOME_PROMPT = "[L]ogin  [N]ew user  [Q]uit:";
    public final String INBOX_COMMANDS = "[S]end message  [R]ead " +
            "messages  [L]ogout:";
    public final String MESSAGE_COMMANDS = "[K]eep  [E]rase:";
    public final String NEXT_MESSAGE_OR_DONE = "[N]ext  [D]one with messages:";
    public final String SEND_TO = "To:";
    public final String MESSAGE_INPUT_PROMPT = "Message text, . to end:";
    public final String SENT = "Message sent.";
    public final String NOT_SENT = "Message not sent.";
    public final String SELECT_NAME_PROMPT = "Select a user name:";
    public final String SELECT_PASS_PROMPT = "Select a password:";
    public final String NEW_USER_ADDED = "User added.";
    public final String NEW_USER_NOT_ADDED = "User not added.";
    public final String LOGIN_NAME_PROMPT = "User name:";
    public final String LOGIN_PASS_PROMPT = "Password:";

    /**
     * Create a new MailSystem
     */
    public MailSystem() {
        loggedIn = false;
        currentUser = null;
        userMailbox = null;
        users = new HashMap<>();
    }

    public boolean login(String name, String password) {
        User u = users.get(name);
        if (u == null) {
            return false;
        }
        boolean correct = u.checkPassword(password);
        if (correct) {
            loggedIn = true;
            currentUser = u;
            userMailbox = u.getMailbox();
        }
        return correct;
    }

    /**
     * Check if there is a current user
     *
     * @return true if there is a user that has logged in
     */
    public boolean isLoggedIn() {
        return loggedIn;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public boolean newUser(String name, String password) {
        if (users.containsKey(name)) return false;
        User aNewUser = new User(name, password);
        users.put(name, aNewUser);
        return true;
    }

    public boolean hasUser(String name) {
        return users.containsKey(name);
    }

    public void sendMailTo(String otherUserName, String theMessage) {
        String composed = "From: " + currentUser.getName();
        composed = composed + "\n" + theMessage;
        User targetUser = users.get(otherUserName);
        Message newMessage = new Message(composed);
        targetUser.getMailbox().addMessage(newMessage);
    }

    public boolean hasCurrentMessage() {
        return userMailbox.hasCurrentMessage();
    }

    public String readCurrentMessage() {
        return userMailbox.getCurrentMessage().getText();
    }

    public String removeCurrentMessage() {
        return userMailbox.removeCurrentMessage().getText();
    }

    public void saveCurrentMessage() {
        userMailbox.saveCurrentMessage();
    }

    public void logout() {
        loggedIn = false;
        currentUser = null;
        userMailbox = null;
    }
}
