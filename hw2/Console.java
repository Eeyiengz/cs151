import java.util.Scanner;

public class Console {
    private Scanner console;

    public Console() {
        console = new Scanner(System.in);
    }

    public void connect(MailSystem mailSystem) {
        boolean more = true;
        while (more) {
            while (!mailSystem.isLoggedIn()) {
                System.out.println(mailSystem
                        .GREETING);
                System.out.println(mailSystem.HOME_PROMPT);
                String alphaInput = console.next().toUpperCase();
                while(!"LNQ".contains(alphaInput)) {
                    System.out.println(mailSystem.HOME_PROMPT);
                    alphaInput = console.next().toUpperCase();
                }
                if (alphaInput.equals("Q")) {
                    more = false;
                    break;
                } else {
                    if (alphaInput.equals("L")) {
                        if (alphaInput.equals("L")) {
                            while (!mailSystem.isLoggedIn()) {
                                System.out.println(mailSystem.LOGIN_NAME_PROMPT);
                                String name = console.next();
                                System.out.println(mailSystem.LOGIN_PASS_PROMPT);
                                String pass = console.next();
                                if (mailSystem.login(name, pass)) {
                                    System.out.println("Welcome " + mailSystem
                                            .getCurrentUser().getName() + ".");
                                } else {
                                    break;
                                }
                            }
                        }
                    } else if (alphaInput.equals("N")) {
                        System.out.println(mailSystem.SELECT_NAME_PROMPT);
                        String newName = console.next().trim();
                        System.out.println(mailSystem.SELECT_PASS_PROMPT);
                        String newPass = console.next().trim();
                        if (mailSystem.newUser(newName, newPass)) {
                            System.out.println(mailSystem.NEW_USER_ADDED);
                        } else {
                            System.out.println(mailSystem.NEW_USER_NOT_ADDED);
                        }
                    }
                }
            }
            while (mailSystem.isLoggedIn()) {
                System.out.println(mailSystem.INBOX_COMMANDS);
                String betaInput = console.next().trim().toUpperCase();
                if (betaInput.equals("S")) {
                    System.out.println(mailSystem.SEND_TO);
                    String sendTo = console.next();
                    System.out.println(mailSystem.MESSAGE_INPUT_PROMPT);
                    String theMessage = "";
                    boolean hasNextLine = true;
                    console.nextLine();
                    while (hasNextLine) {
                        String composingMessage = console.nextLine();
                        if (!composingMessage.equals(".")) {
                            theMessage = theMessage + composingMessage + "\n";
                        } else {
                            theMessage = theMessage.trim();
                            hasNextLine = false;
                        }
                    }
                    if (mailSystem.hasUser(sendTo)) {
                        mailSystem.sendMailTo(sendTo, theMessage);
                        System.out.println(mailSystem.SENT);
                    } else {
                        System.out.println(mailSystem.NOT_SENT);
                    }
                } else if (betaInput.equals("R")) {
                    boolean done = false;
                    if (!mailSystem.hasCurrentMessage()) done = true;
                    while (!done) {
                        System.out.println(mailSystem.readCurrentMessage());
                        System.out.println(mailSystem.MESSAGE_COMMANDS);
                        String gammaInput = console.next().toUpperCase();
                        while (true) {
                            if (gammaInput.equals("K")) {
                                mailSystem.saveCurrentMessage();
                                break;
                            }
                            if (gammaInput.equals("E")) {
                                mailSystem.removeCurrentMessage();
                                break;
                            }
                        }
                        System.out.println(mailSystem.NEXT_MESSAGE_OR_DONE);
                        String deltaInput = console.next().toUpperCase();
                        while (true) {
                            if (deltaInput.equals("N") && mailSystem
                                    .hasCurrentMessage()) {
                                break;
                            }
                            if (deltaInput.equals("D") || !mailSystem
                                    .hasCurrentMessage()) {
                                done = true;
                                break;
                            }
                        }
                    }
                } else if (betaInput.equals("L")) {
                    mailSystem.logout();
                } else {
                    // continue
                }
            }
        }
    }
}