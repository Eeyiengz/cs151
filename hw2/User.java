/**
 * User class emulate a user profile
 * Log in to access data
 */
public class User {
    private String name;
    private String password;
    private Mailbox mBox;

    /**
     * Create a new User
     * User has a name and a password
     *
     * @param name     the user's name
     * @param password the user's password
     */
    public User(String name, String password) {
        this.name = name;
        this.password = password;
        this.mBox = new Mailbox();
    }

    /**
     * return the user's name
     *
     * @return the user's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Check if the password is correct.
     *
     * @param password a password to check
     * @return true if the supplied password matches the User password
     */
    public boolean checkPassword(String password) {
        return password.equals(this.password);
    }

    public Mailbox getMailbox() {
        return mBox;
    }

    /**
     * Change User's password.
     *
     * @param newPassword the new password
     */
    public void setPassword(String newPassword) {
        password = newPassword;
    }
}