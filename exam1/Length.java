/**
   Length, measured the good old American way, in feet and inches.
*/
public class Length
{
   /**
      Constructs a Length object.
      @param feet the given number of feet, >= 0
      @param inches the given number of inches, >= 0
    */
   public Length(int feet, int inches)
   {
      assert feet >= 0;
      assert inches >= 0;

      this.inches = (feet * 12) + (inches);
   }

   /**
      The number of feet in this length, when normalized so
      that the number of inches is < 12.
      @return the number of feet
   */
   public int getFeet()
   {
      return this.inches/12;
   }

   /**
      The number of inches in this length, when normalized so
      that the number of inches is < 12.
      @return the number of inches
   */
   public int getInches()
   {
      return inches % 12;
   }

   /**
      Adds two lengths.
      @param other another length
      @param the sum of this and other
   */
   public Length sum(Length other)
   {
      return new Length(inches / 12 + other.inches/12, inches % 12 + other.inches %12);
   }

   /**
      Subtracts two lengths.
      @param other another length
      @param the result of subtracting the smaller of the two
      lengths from the larger
   */
   public Length difference(Length other)
   {
      int diff = Math.abs(this.inches - other.inches);
      int feet = diff / 12;
      int inch = diff %12;
      return new Length(feet,inch);
   }

   private int inches;
}
