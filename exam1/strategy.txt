Name in Design Pattern						Actual Name

Context										FileFilter in JFileChooser 
Strategy									filter out specified filenames
ConcreteStrategy							FileNameExtensionFilter("filenames")
doWork()									filter out "filenames"