
public class OneTimeText implements Text{
    public OneTimeText(String t) {
        text = t;
    }

    @Override
    public String getText() {
        String temp = text;
        text = "";
        return temp;

    }

    private String text;
}
