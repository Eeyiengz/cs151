/**
 * A message left by the caller.
 */
public class Message {
    /**
     * Construct a Message object.
     *
     * @param messageText the message text
     */
    public Message(String messageText, boolean oneTimeText) {
        if(oneTimeText) text = new OneTimeText(messageText);
        else text = new PlainText(messageText);
    }

    /**
     * Get the message text.
     *
     * @return message text
     */
    public String getText() {
        return text.getText();
    }

    private Text text;
}
