
public class PlainText implements Text{

    public PlainText(String t) {
        text = t;
    }
    @Override
    public String getText() {
        return text;
    }

    private String text;
}
