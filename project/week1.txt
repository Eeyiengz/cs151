October 22, 2016 4:30pm - 6:17pm: 
    started the project repo
    read markdown tutorials
    outlined the wiki
    
October 24, 2016 11:30pm - 2:36am:
    springy layout of Violet
    
October 25, 2016 10:50pm - 12:20am
    learning/testing Apache Ant on Violet
    continue reading about reflection and annotation

October 26, 2016 12:15pm - 1:50pm
    resumed reading
    
Total hours this week: 8
Total hours on project: 8

-------------------------
Comments: 
    This project has been slow to start because of all the new stuff we need to know. My teammates all agree that this project is the most confusing one. It is hard to envision the final product (RoundTripViolet) compared to the other projects.
    