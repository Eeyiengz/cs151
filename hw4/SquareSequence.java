import java.util.function.LongPredicate;

/**
 * an infinite sequence consisting of perfect squares
 */
public class SquareSequence implements NumberSequence {

    /**
     * construct a new SquareSequence
     */
    public SquareSequence() {
        base = 0;
        sum = 0;
        op = 0;
        p = null;
    }

    /**
     * returns the next perfect square number
     */
    @Override
    public long next() {
        if (p == null) {
            long squared = base * base;
            base++;
            sum += squared;
            return squared;
        } else {
            long squared = base * base;
            base++;
            if (p.test(squared)) {
                sum += squared;
                op++;
                return squared;
            }
            else {
                return this.next();
            }
        }
    }

    /**
     * compute the average of all squares so far
     * @return the average
     */
    @Override
    public double average() {
        return sum / op;
    }

    /**
     * filter the sequence
     * @param p the predicate
     * @return a new filtered sequence
     */
    @Override
    public NumberSequence filter(LongPredicate p) {
        return new SquareSequence(p);
    }

    /**
     * constructs a filtered sequence
     * @param p the predicate
     */
    private SquareSequence(LongPredicate p) {
        base = 0;
        sum = 0;
        op = 0;
        this.p = p;
    }

    private long base;
    private double sum;
    private double op;
    private LongPredicate p;
}
