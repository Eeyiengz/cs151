import java.util.function.LongPredicate;
import java.util.function.LongUnaryOperator;

/**
 * NumberSequence interface represents a finite or infinite sequence of integers
 */
public interface NumberSequence {

    /**
     * gives the next element in the sequence
     * @return the next element
     */
    long next();

    /**
     * constructs a finite sequence given by the argument
     * param args the inputs
     * @return a new finite sequence
     */
    static NumberSequence of(long... args) {
        return new CustomSequence(args);
    }

    /**
     * gives the average of the sequence
     * @return the average
     */
    double average();

    /**
     * determine is there is a next element
     * @return true if there is a next element
     */
    default boolean hasNext() {
        return true;
    }

    /**
     * computes the average up to element n
     * @param n the upper-bound to compute
     * @return the average up to element n
     */
    default double average(int n) {
        double sum = 0;
        double i;
        for (i = 0; i < n && this.hasNext(); i++) {
            sum += this.next();
        }
        return sum / i;
    }

    /**
     * return the sequence in a primitive array up to element n
     * @param n copy up to this upper-bound
     * @return primitive long array of elements up to n
     */
    default long[] toArray(int n) {
        long[] tempArr = new long[n];
        int i;
        for (i = 0; this.hasNext() && i < n; i++) {
            tempArr[i] = this.next();
        }
        long[] retArr = new long[i];
        System.arraycopy(tempArr, 0, retArr, 0, retArr.length);
        return retArr;
    }

    /**
     * filter a NumberSequence object subjected to a given predicate
     * @param p the predicate
     * @return a filtered sequence
     */
    default NumberSequence filter(LongPredicate p) {
        int bucket = 10;
        int loc = 0;
        long[] larr = new long[bucket];

        while (this.hasNext()) {
            long l = this.next();
            if (p.test(l)) {
                if (loc >= bucket) {
                    bucket *= 2;
                    long[] newArr = new long[bucket];
                    int newLoc = 0;
                    for (long j : larr) {
                        newArr[newLoc] = j;
                        newLoc++;
                    }
                    larr = newArr;
                }
                larr[loc] = l;
            }
            loc++;
        }
        long[] newLarr = new long[loc];
        System.arraycopy(larr, 0, newLarr, 0, loc);
        return of(newLarr);
    }

    /**
     * constructs a custom sequence
     */
    class CustomSequence implements NumberSequence {
        public CustomSequence(long[] args) {
            loc = 0;
            larr = args;
            this.f = null;
        }

        public CustomSequence(long seed, LongUnaryOperator f) {
            loc = 0;
            larr = new long[10];
            this.f = f;
            this.seed = seed;
        }

        public long next() {
            loc++;
            if (f != null) {
                long l = seed;
                seed = f.applyAsLong(seed);
                larr[loc - 1] = l;
                return l;
            } else {
                return larr[loc - 1];
            }
        }

        public boolean hasNext() {
            if (larr == null) return false;
            return loc < larr.length;
        }

        public double average() {
            double sum = 0;
            for (double l : larr) {
                sum += l;
            }
            return sum / (double) larr.length;
        }

        private CustomSequence(long[] args, long seed, LongUnaryOperator f) {
            loc = 0;
            larr = args;
            this.f = f;
        }

        private long[] larr;
        private int loc;
        private LongUnaryOperator f;
        private long seed;
    }

    /**
     * construct an infinite sequence according to a given rule
     * @param seed original seed
     * @param f the operator
     * @return an iterative number sequence bounded by the operator
     */
    static NumberSequence iterate(long seed, LongUnaryOperator f) {
        return new CustomSequence(seed, f);
    }

    /**
     * return a random number using linear congruential generator
     * @param seed original seed
     * @return a random number
     */
    static NumberSequence random(long seed) {
        long nextSeed = seed;
        long m = 2147483648L;
        long a = 1103515245L;
        long c = 12345L;
        LongUnaryOperator f = x -> (a * x + c) % m;
        return iterate(seed, f);
    }
}