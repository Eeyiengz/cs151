package controllers;

import play.mvc.*;
import java.util.HashMap;

import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public Result select(String problem, String choice) {
    	results.compute(problem, (k, v) -> v == null ? new HashMap<>() : v)
    	   .compute(choice, (k, v) -> v == null ? 1 : v + 1);
    	   return ok("You selected " + choice + " for " + problem + "\n").as("text/plain");
    	}
    
    public Result view(String question) {
    	return ok(
    }
    
    private static HashMap<String, HashMap<String, Integer>> results = new HashMap<>();
}
