import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.*;

/**
 * a shape decorator
 * draws a box around a shape with paddings on all sides
 */
public class BoxedShape implements Shape, Serializable, Cloneable {
    /**
     * create a boxed shape
     *
     * @param shape   the shape
     * @param padding amount of padding
     */
    public BoxedShape(Shape shape, int padding) {
        this.shape = shape;
        this.padding = padding + 1;
    }

    @Override
    public boolean contains(double x, double y) {
        return shape.contains(x, y);
    }

    @Override
    public boolean contains(double x, double y, double w, double h) {
        return shape.contains(x, y, w, h);
    }

    @Override
    public boolean contains(Point2D p) {
        return shape.contains(p);
    }

    @Override
    public boolean contains(Rectangle2D r) {
        return shape.contains(r);
    }

    @Override
    public Rectangle getBounds() {
        Rectangle bound = shape.getBounds();
        return new Rectangle(bound.x - padding, bound.y - padding, bound.width + padding * 2, bound.height + padding * 2).getBounds();
    }

    @Override
    public Rectangle2D getBounds2D() {
        Rectangle2D bound = shape.getBounds2D();
        return new Rectangle.Double(bound.getX() - padding, bound.getY() - padding, bound.getWidth() + padding * 2, bound.getHeight() + padding * 2).getBounds();
    }

    @Override
    public boolean intersects(double x, double y, double w, double h) {
        return shape.intersects(x, y, w, h);
    }

    @Override
    public boolean intersects(Rectangle2D r) {
        return shape.intersects(r);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at) {
        return new BoxedShapePathIterator(shape, padding, at);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return new BoxedShapePathIterator(shape, padding, at, flatness);
    }

    public BoxedShape clone() {
        BoxedShape cloned = null;
        try {
            cloned = (BoxedShape) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
        return cloned;
    }

    public ObjectOutputStream writeObject() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("BoxedShape.dat"));
            out.writeObject((Object) this);
            out.close();
            return out;
        } catch (FileNotFoundException fe) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    private Shape shape;
    private int padding;
}