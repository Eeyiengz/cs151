import java.awt.*;
import java.time.Duration;
import java.time.Instant;

/**
 * A class emulating a stopwatch
 */
public class Stopwatch implements MoveableShape {
    /**
     * constructs the Stopwatch
     *
     * @param radius its radius
     */
    public Stopwatch(int radius) {
        this.radius = radius;
        this.bigD = new Dial(radius, true, Color.RED);
        this.smallD = new Dial(2 * radius / 5, false, Color.MAGENTA);
        this.running = false;
        this.frozen = false;
        this.stopped = true;
    }

    @Override
    public void draw(Graphics2D g2) {
        bigD.paintIcon(null, g2, 0, 0);
        smallD.paintIcon(null, g2, radius - 2 * radius / 5, radius - 3 *
                radius / 5);
    }

    @Override
    public void move() {
        if (start == null && running) {
            start = Instant.now();
        } else {
            if (running && !stopped) {
                if (!frozen) {
                    double NANO_IN_SEC = 1e9;
                    Instant now = java.time.Instant.now();

                    double nano = (double) Duration.between(start, now).toNanos();
                    double handAngle = (nano / (NANO_IN_SEC * 60.0)) * (2 * Math.PI);
                    updateHand(handAngle);
                }
            }
        }
    }

    /**
     * notify that the 'top' is was pressed
     * perform actions associated with the behavior of a stopwatch 'top' button
     */
    public void topButtonPressed() {
        if (!frozen) {
            if (!running && stopped) {
                running = true;
                stopped = false;
            } else if (running) {
                stopped = !stopped;
                if (stopped) timeDiff = Instant.now();
                if (!stopped) start = start.plusNanos(Duration.between(timeDiff,
                        Instant.now()).toNanos());
            }
        }
    }

    /**
     * notify that the 'second' is was pressed
     * perform actions associated with the behavior of a stopwatch 'second'
     * button
     */
    public void secondButtonPressed() {
        if (stopped) {
            start = null;
            running = false;
            updateHand(0);
        } else {
            frozen = !frozen;
        }
    }

    /**
     * update the Dial's hand angles
     *
     * @param angle the angle
     */
    private void updateHand(double angle) {
        bigD.updateHandAngle(angle);
        smallD.updateHandAngle(angle);
    }

    private final int radius;
    private Dial bigD;
    private Dial smallD;
    private Instant start;
    private boolean running;
    private boolean frozen;
    private boolean stopped;
    private Instant timeDiff;
}