
import java.awt.*;
import java.util.*;
import javax.swing.*;

/**
 * An icon that contains a moveable shape.
 */
public class ShapeIcon implements Icon {
    /**
     * constructs a ShapeIcon
     *
     * @param shape  the shape
     * @param width  its width
     * @param height its height
     */
    public ShapeIcon(MoveableShape shape,
                     int width, int height) {
        this.shape = shape;
        this.width = width;
        this.height = height;
    }

    /**
     * get the width
     *
     * @return the width
     */
    public int getIconWidth() {
        return width;
    }

    /**
     * get the height
     *
     * @return the height
     */
    public int getIconHeight() {
        return height;
    }

    /**
     * paint the icon at specified location
     *
     * @param c the optional component
     * @param g Graphics library
     * @param x horizontal coordinate
     * @param y vertical coordinate
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g;
        shape.draw(g2);
    }

    private int width;
    private int height;
    private MoveableShape shape;
}