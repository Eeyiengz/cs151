import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;

/**
 * A class that draw a clock face at coordinate x, y
 */
public class Dial implements Icon {
    /**
     * constructs the Dial with i radius, and color c
     *
     * @param i the radius
     * @param b tick number options. True for three ticks
     * @param c the color of the hand
     */
    public Dial(int i, boolean b, Color c) {
        this.radius = i;
        this.threeLevels = b;
        this.color = c;
        this.tickAngle = -1 * Math.PI / 2;
        this.handAngle = -1 * Math.PI / 2;
    }

    /**
     * paint the Dial at coordinate x, y
     *
     * @param c the extra component to define the painting behavior. Optional
     * @param g the Java Graphics library
     * @param x the x coordinate
     * @param y the y coordinate
     */
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setFont(new Font(null, Font.BOLD, radius *
                13 / 100));
        drawTicks(g2, x + radius, y + radius);
        // draw hand with color
        g2.setPaint(color);
        g2.fill(drawHand(g2, radius, radius + x, radius + y));

    }

    /**
     * get the icon width
     *
     * @return the width
     */
    @Override
    public int getIconWidth() {
        return radius * 2;
    }

    /**
     * get the icon height
     *
     * @return the height
     */
    @Override
    public int getIconHeight() {
        return radius * 2;
    }

    /**
     * update the angle at which the hand will be drawn
     *
     * @param angle the angle
     */
    public void updateHandAngle(double angle) {
        if (threeLevels) handAngle = (-1 * Math.PI / 2) + angle;
        else handAngle = (-1 * Math.PI / 2) + (angle / 60.0);
    }

    /**
     * draw a tick at the specified displacement value
     *
     * @param g2 the Java Graphics2D library
     * @param dx horizontal displacement value
     * @param dy vertical displacement value
     */
    private void drawTicks(Graphics2D g2, int dx, int dy) {
        // obtain scaling values
        double TICK_LEVEL_LENGTH = TICKS_LENGTH_SCALING * ((double) radius / 400.0);
        double inner = radius - 3 * TICK_LEVEL_LENGTH;
        double middle = radius - 2 * TICK_LEVEL_LENGTH;
        double outer = radius - TICK_LEVEL_LENGTH;

        int tickCount = 0;

        // calculate where to draw, and draw it
        Point2D.Double start;
        Point2D.Double end;
        g2.setPaint(Color.BLACK);
        while (tickCount < 300) {
            if (tickCount % 25 == 0) {
                int numDisp = (radius * 15 / 100) / 2;
                start = getPointAt(getTickAngle(), inner, dx, dy);
                drawNumbers(g2, tickCount / 5, dx - numDisp, dy + numDisp);
            } else if (tickCount % 5 == 0) {
                start = getPointAt(getTickAngle(), middle, dx, dy);
            } else {
                start = getPointAt(getTickAngle(), outer, dx, dy);
            }
            end = getPointAt(getTickAngle(), radius, dx, dy);

            Shape segment = new Line2D.Double(start, end);
            g2.draw(segment);

            if (threeLevels) tickCount++;
            else {
                tickCount += 5;
            }
            nextTick();
        }


    }

    /**
     * draw the Dial's moving hand and return the Path2D Object to be painted
     *
     * @param g2     Java Graphics2D library
     * @param length the length of the hand
     * @param dx     horizontal displacement value
     * @param dy     vertical displacement value
     * @return Path2D of the arrowhead
     */
    private Path2D drawHand(Graphics2D g2, int length, int dx, int dy) {
        // borrowed from ArrowHead with modifications //
        double ARROW_ANGLE = Math.PI / 7.5;
        double ARROW_LENGTH = 23 * ((double) radius / 400.0); // scale

        Point2D tail = getPointAt(handAngle, length - ARROW_LENGTH, dx,
                dy);
        Point2D head = getPointAt(handAngle, length - 2 * ARROW_LENGTH,
                dx, dy);
        Point2D center = new Point2D.Double(radius, radius);
        if (!threeLevels) center.setLocation(radius * 5 / 2, 2 * radius);
        Shape segment = new Line2D.Double(center, tail);
        g2.draw(segment);

        Path2D path = new GeneralPath();

        double disx = tail.getX() - head.getX();
        double disy = tail.getY() - head.getY();
        double angle = Math.atan2(disy, disx);
        double x1 = tail.getX()
                - ARROW_LENGTH * Math.cos(angle + ARROW_ANGLE);
        double y1 = tail.getY()
                - ARROW_LENGTH * Math.sin(angle + ARROW_ANGLE);
        double x2 = tail.getX()
                - ARROW_LENGTH * Math.cos(angle - ARROW_ANGLE);
        double y2 = tail.getY()
                - ARROW_LENGTH * Math.sin(angle - ARROW_ANGLE);
        path.moveTo(tail.getX(), tail.getY());
        path.lineTo(x1, y1);
        path.lineTo(x2, y2);
        path.closePath();
        // borrowed from ArrowHead with modifications//

        return path;
    }

    /**
     * draw number at a specified coordinate
     *
     * @param g2     Java Graphics2D library
     * @param number the number to draw
     * @param dx     horizontal displacement value
     * @param dy     vertical displacement value
     */
    private void drawNumbers(Graphics2D g2, int number, int dx, int dy) {
        double at = radius - 5 * TICKS_LENGTH_SCALING * ((double) radius / 400.0);
        Point2D p = getPointAt(getTickAngle(), at, dx, dy - Math.round(10.0f * (radius / 400.0f)));
        String pad = "";
        if (number == 5 || number == 0) pad = " ";
        g2.drawString(pad + number, Math.round(p.getX()), Math.round(p
                .getY()));
    }

    /**
     * get the current tick's angle value
     *
     * @return the angle
     */
    private double getTickAngle() {
        return tickAngle;
    }

    /**
     * calculate the next tick's angle
     *
     * @return the angle
     */
    private double nextTick() {
        double TICK_ANGLE = 2 * Math.PI / 300.0;
        if (threeLevels) tickAngle += TICK_ANGLE;
        else tickAngle = tickAngle + (5 * TICK_ANGLE);
        return tickAngle;
    }

    /**
     * get a Point2D object with specified values
     *
     * @param angle         the angle of the a unit vector starting at
     *                      90 degrees and going clockwise
     * @param scale         the distance from the origin to the new Point2D object
     * @param displacementX horizontal displacement
     * @param displacementY vertical displacement
     * @return the new Point2D object
     */
    private Point2D.Double getPointAt(double angle, double scale, int
            displacementX, int displacementY) {
        double x = Math.cos(angle) * scale;
        double y = Math.sin(angle) * scale;
        return new Point2D.Double(x + displacementX, y + displacementY);
    }


    private double tickAngle;
    private int radius;
    private boolean threeLevels;
    private Color color;
    private double handAngle;

    private final int TICKS_LENGTH_SCALING = 20;
}
