import java.math.BigDecimal;

/**
 * Created by eeyie on 11/10/2016.
 */
public class PairTester {
    public static void main(String[] args) {
        Pair<BigDecimal> p1 = new Pair<BigDecimal>();
        LabeledDecimal ld1 = new LabeledDecimal("pi", "3.14");
        LabeledDecimal ld2 = new LabeledDecimal("sqrt(2)", "1.414");
        Pair<LabeledDecimal> p2 = new Pair<LabeledDecimal>(ld1, ld2);
        p1.copyFrom(p2);
        System.out.println(p1);


        LabeledDecimal lds1 = new LabeledDecimal("pi", "3.14");
        LabeledDecimal lds2 = new LabeledDecimal("sqrt(2)", "1.414");
        Pair<LabeledDecimal> ps1 = new Pair<LabeledDecimal>(lds1, lds2);
        Pair<BigDecimal> ps2 = new Pair<BigDecimal>();
        p1.copyTo(ps2);
        System.out.println(ps2);

        Pair<BigDecimal> sp1 = new Pair<BigDecimal>(new BigDecimal("3.14"),
                new BigDecimal("1.414"));
    }
}
