import java.util.Comparator;

public class Pair<T>
{
    public Pair() { first = null; second = null; }
    public Pair(T first, T second) { this.first = first;  this.second = second; }

    public T get(int n) { return n == 0 ? first : n == 1 ? second : null; }

    public void set(int n, T t)
    {
        if (n == 0) first = t;
        else if (n == 1) second = t;
    }

    public void copyFrom(Pair<? extends T> n1){
        this.first =  n1.first;
        this.second = n1.second;
    }

    public void copyTo(Pair<? super T> n2) {
        n2.first = this.first;
        n2.second = this.second;
    }

    public T min(Comparator<T> comp)
    {
        if (comp.compare(first, second) < 0)
            return first;
        else
            return second;
    }

    private T first;
    private T second;
}