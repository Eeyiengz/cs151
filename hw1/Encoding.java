import java.util.Set;
import java.util.TreeSet;

public class Encoding {
    public static Set<String> morseCodes(int m, int n) {
        Set<String> result = new TreeSet<>();

        result.addAll(morseHelper("", m, n));
        return result;
    }

    private static Set<String> morseHelper(String str, int m, int n) {
        Set<String> newSet = new TreeSet<>();
        if (m == 0 && n == 0) {
            newSet.add(str);
            return newSet;
        }
        if (m > 0) newSet.addAll(morseHelper(str.concat("."), m - 1, n));
        if (n > 0) newSet.addAll(morseHelper(str.concat("-"), m, n - 1));
        return newSet;
    }
}
