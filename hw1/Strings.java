public class Strings {
    public static String uniqueLetters(String str) {
        return uniqueLettersHelper(str, "");
    }

    private static String uniqueLettersHelper(String str, String answer) {
        if (str.isEmpty()) return answer;
        String firstLetter = str.substring(0, 1);
        if (!str.substring(1).contains(firstLetter)) answer += firstLetter;
        return uniqueLettersHelper(str.replace(firstLetter, ""), answer);
    }
}