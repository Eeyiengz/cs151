Student: zheng_eeyieng
At least 3 git commits (max. 5 points): 5
DayWithTime compiles, passes tests (max. 10 points): 10
MutableDay compiles, passes tests (max. 10 points): 10
 
DayWithTimeTest.java
JUnit version 4.11
.......
Time: 0.013

OK (7 tests)

MutableDayTest.java
JUnit version 4.11
........
Time: 0.016

OK (8 tests)

 
commit 53d080c4a2a294163cfbe79c7bae0cc1daac8c5d
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Wed Sep 21 22:06:28 2016 -0700

    took out tests from git

:100644 000000 a62a28f... 0000000... D	hw3/DayWithTimeTest.java
:100644 000000 78412db... 0000000... D	hw3/MutableDayTest.java

commit a74567bfbc4625983646787f3fd68843e84e9f67
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Wed Sep 21 22:04:05 2016 -0700

    added constructor check

:100644 100644 936040a... 5f523d7... M	hw3/MutableDay.java

commit 93a13a5086fc89d768ebd1cdf54026746999ca52
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Tue Sep 20 16:50:08 2016 -0700

    Completed. Passed all given tests. No custom tests.

:100644 100644 ef72d91... 936040a... M	hw3/MutableDay.java
:100644 100644 1a705ed... 78412db... M	hw3/MutableDayTest.java

commit 2587b4fd3509beafbadb6fedb9ccfaa2db48c0ee
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Mon Sep 19 15:24:26 2016 -0700

    completed DayWithTime. Passed all test cases from DayWithTimeTest. Did not test with custom test cases

:100644 100644 8b2ec87... a3bc89e... M	hw3/DayWithTime.java
:100644 100644 53aeee5... ef72d91... M	hw3/MutableDay.java

commit 2031939b395f72b02758fbd7e722c1ee30c25122
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Sun Sep 18 23:06:40 2016 -0700

    progressing

:100644 100644 f655a65... 104a433... M	.gitignore
:100644 100644 0d43b91... 8b2ec87... M	hw3/DayWithTime.java
:100644 100644 0b63223... 1a705ed... M	hw3/MutableDayTest.java

commit 9f20376232f63ba89da3ffc30a02e7397d81483e
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Fri Sep 16 23:02:17 2016 -0700

    baseline

:100644 100644 51a8885... f655a65... M	.gitignore
:000000 100644 0000000... 0d43b91... A	hw3/DayWithTime.java
:000000 100644 0000000... a62a28f... A	hw3/DayWithTimeTest.java
:000000 100644 0000000... 53aeee5... A	hw3/MutableDay.java
:000000 100644 0000000... 0b63223... A	hw3/MutableDayTest.java

commit c81f591102c84e541f36a368c9635a741b8bc5f8
Author: Ee Yieng Zheng <eeyiengz@hotmail.com>
Date:   Fri Sep 16 22:55:22 2016 -0700

    ...

:100644 100644 9fdde2d... 51a8885... M	.gitignore
:100644 100644 29bc23c... a350392... M	hw1scores.txt
:100644 100644 4663af8... b403619... M	hw2/Console.java
:100644 100644 2b4de61... 57709b9... M	hw2/MailSystem.java
:100644 100644 90498e0... 331dd82... M	hw2/MailSystemTester.java
:100644 100644 6fe6b3b... 77052db... M	hw2/Mailbox.java
:100644 100644 948c64c... 17330b4... M	hw2/Message.java
:100644 100644 6cc61ea... d066ec9... M	hw2/MessageQueue.java
:100644 100644 6021748... a4c1a65... M	hw2/User.java
:100644 100644 b2edd84... a70ca11... M	hw2/crc.txt
:100644 100644 3c3beb1... 0653c0b... M	hw2/uml.txt
:100644 100644 9cd9055... 6c630f5... M	lab2/report.txt

commit 46ffe03a56a59b7535425cdf875b0ffc4a04953b
Author: Cay Horstmann <cay@horstmann.com>
Date:   Fri Sep 16 21:57:10 2016 -0700

    Added hw2 scores

:000000 100644 0000000... f5bb03b... A	hw2ascores.txt
:000000 100644 0000000... e4ac61a... A	hw2bscores.txt
 
