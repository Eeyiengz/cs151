import java.math.BigInteger;
import java.util.concurrent.*;

/**
 * Created by eeyie on 11/22/2016.
 */
public class Primes {
    private  static long nonprime = 0;

    public static Runnable printPrimes(BigInteger start, long length) {
        return () ->
        {
            BigInteger n = start;
            for (long i = 0; i < length; i++) {
                if (n.isProbablePrime(100))
                    System.out.println(n);
                n = n.add(BigInteger.ONE);
            }
        };
    }

    public static Callable<Long> countPrimes(BigInteger start, long l) {
        return () -> {
            BigInteger n = start;
            long count = 0;
            for (long i = 0; i < l; i++) {
                if (n.isProbablePrime(100)) count++;
                else nonprime++;
                n = n.add(BigInteger.ONE);
            }
            return count;
        };
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Long> c1 = countPrimes(new BigInteger("1000000000000000"), 500_000);
        Callable<Long> c2 = countPrimes(new BigInteger("1000000000500000"), 500_000);

        int processors = Runtime.getRuntime().availableProcessors();
        ExecutorService service = Executors.newFixedThreadPool(processors);

        Future<Long> f1 = service.submit(c1);
        Future<Long> f2 = service.submit(c2);
        long start = System.currentTimeMillis();
        System.out.println("f1 primes = " + f1.get());
        System.out.println("f2 primes = " + f2.get());
        System.out.println("Nonprime = " + nonprime);
        long end = System.currentTimeMillis();
        System.out.println("Milliseconds: " + (end - start));
        service.shutdown();
    }
}
