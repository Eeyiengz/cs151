import java.awt.Font;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;

import javax.swing.JOptionPane;

/**
 * TextShapeMaker
 */
public class TextShapeMaker
{
   /**
    * make the text
    *
    * @param message the message
    * @param f       Font type
    * @return the Shape
    */
   public static Shape makeShape(String message, Font f)
   {
      FontRenderContext context = new FontRenderContext()
      {
      };
      TextLayout layout = new TextLayout(message, f, context);
      Shape outline = layout.getOutline(context.getTransform());
      return outline;
   }

   /**
    * run a tester
    *
    * @param args the args
    */
   public static void main(String[] args)
   {
      JOptionPane.showMessageDialog(null, // parent window
         "", // message
         "", // window title
         JOptionPane.INFORMATION_MESSAGE, // message type
         new ShapeIcon(makeShape("Hello", new Font("Serif", Font.PLAIN, 100))));
   }
}
