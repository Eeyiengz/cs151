import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * an Icon with other Icons bundled together
 */
public class CompositeIcon implements Icon
{
   /**
    * create a new CompositeIcon
    */
   public CompositeIcon()
   {
      icons = new ArrayList<>();
      this.maxHeight = 0;
      this.maxWidth = 0;
   }

   /**
    * add to the CompositeIcon an Icon
    *
    * @param icon the icon to add
    */
   public void add(Icon icon)
   {
      if (icon.getIconHeight() > maxHeight) this.maxHeight = icon.getIconHeight();
      if (icon.getIconWidth() > maxWidth) this.maxWidth = icon.getIconWidth();
      icons.add(icon);
   }

   @Override
   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      for (Icon icon : icons)
      {
         icon.paintIcon(c, g, x, y);
      }
   }

   @Override
   public int getIconWidth()
   {
      return maxWidth;
   }

   @Override
   public int getIconHeight()
   {
      return maxHeight;
   }

   private ArrayList<Icon> icons;
   private int maxWidth;
   private int maxHeight;
}
