import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * an Icon decorator
 * draw a box around an icon
 */
public class BoxedIcon implements Icon
{
   /**
    * decorate the Icon operant
    *
    * @param icon    the Icon
    * @param padding blank pixels from the Icon border
    */
   public BoxedIcon(Icon icon, int padding)
   {
      this.icon = icon;
      this.padding = padding + 1;
   }

   @Override
   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      Graphics2D g2 = (Graphics2D) g;
      int w = icon.getIconWidth() + 2 * padding;
      int h = icon.getIconHeight() + 2 * padding;

      Shape rect = new Rectangle(x, y, w - 1, h - 1);
      g2.draw(rect);

      icon.paintIcon(c, g, x + padding, y + padding);
   }

   @Override
   public int getIconWidth()
   {
      return icon.getIconWidth() + padding * 2;
   }

   @Override
   public int getIconHeight()
   {
      return icon.getIconHeight() + padding * 2;
   }

   private Icon icon;
   private int padding;
}