import java.awt.*;
import javax.swing.*;

/**
 * a ShiftedShape
 */
public class ShiftedIcon implements Icon
{
   /**
    * create a ShiftedShape
    *
    * @param icon the icon
    * @param x    at x
    * @param y    at y
    */
   public ShiftedIcon(Icon icon, int x, int y)
   {
      this.icon = icon;
      this.shiftx = x;
      this.shifty = y;
   }

   @Override
   public int getIconWidth()
   {
      return icon.getIconWidth() + shiftx;
   }

   @Override
   public int getIconHeight()
   {
      return icon.getIconHeight() + shifty;
   }

   @Override
   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      icon.paintIcon(c, g, x + shiftx, y + shifty);
   }

   private Icon icon;
   private int shiftx;
   private int shifty;
}
