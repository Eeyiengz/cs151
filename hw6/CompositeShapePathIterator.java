import java.awt.geom.PathIterator;
import java.util.ArrayList;

/**
 * iterate through the paths of a CompositeShape
 */
public class CompositeShapePathIterator implements PathIterator
{
   /**
    * create a CompositeShape iterator
    */
   public CompositeShapePathIterator()
   {
      this.iterators = new ArrayList<>();
      currentIter = null;
   }

   /**
    * add a shape's PathIterator to this
    *
    * @param pathIterator the PathIterator
    */
   public void add(PathIterator pathIterator)
   {
      if (currentIter == null) currentIter = pathIterator;
      else iterators.add(pathIterator);
   }

   @Override
   public int getWindingRule()
   {
      return currentIter.getWindingRule();
   }

   @Override
   public boolean isDone()
   {
      if (currentIter.isDone())
      {
         if (iterators.size() == 0) return true;
         currentIter = iterators.remove(0);
      }
      return currentIter.isDone();
   }

   @Override
   public void next()
   {
      currentIter.next();
   }

   @Override
   public int currentSegment(float[] coords)
   {
      return currentIter.currentSegment(coords);
   }

   @Override
   public int currentSegment(double[] coords)
   {
      return currentIter.currentSegment(coords);
   }

   private ArrayList<PathIterator> iterators;
   private PathIterator currentIter;
}
